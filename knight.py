# Global variables
knight_moves = [[-2, -1], [-1, -2], [1, -2], [2, -1], [-2, 1], [-1, 2], [1, 2], [2, 1]]
board_size = 8

class Node:
    def __init__(self, x, y, moves=0):
        self.x = x
        self.y = y
        self.moves = moves

    # Gets the x coordinate of knight after move
    def get_x(self, index) -> int:
        return self.x + knight_moves[index][0]


    # Gets the y coordinate of knight after move
    def get_y(self, index) -> int:
        return self.y + knight_moves[index][1]

    
class Position:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Used in calculation function. Checks if position is in board
    def is_valid(self) -> bool:
        return self.x >= 1 and self.x <= board_size and self.y >= 1 and self.y <= board_size
    

# Prepares matrix for calculations
def prepare_matrix(current_position: Position) -> [[]]:
    visited = [[False for i in range(board_size + 1)]
               for j in range(board_size + 1)]

    visited[current_position.x][current_position.y] = True

    return visited


# Calculates minimum steps for knight to reach selected target (uses breadth-first-search algorithm)
def min_moves_bfs(current_position: Position, target: Position) -> None:
    queue = []
    queue.append(Node(current_position.x, current_position.y))

    visited = prepare_matrix(current_position)

    while len(queue) > 0:
        current = queue.pop(0)

        if current.x == target.x and current.y == target.y:
            return current.moves

        for i in range(8):
            x = current.get_x(i)
            y = current.get_y(i)

            position = Position(x, y)
            
            if position.is_valid() and not visited[position.x][position.y]:
                visited[position.x][position.y] = True
                queue.append(Node(position.x, position.y, current.moves + 1))


# Converts user input to understandable format for calculation function
def convert_positions(knight_position: str, target_position: str) -> [[]]:
    position_list = []

    letters = 'abcdefgh'

    number_knight = letters.index(knight_position[0]) + 1
    number_target = letters.index(target_position[0]) + 1

    current = Position(number_knight, int(knight_position[1]))
    target = Position(number_target, int(target_position[1]))

    position_list.append(current)
    position_list.append(target)

    return position_list


# Checks if user input was in the correct format
def position_input_valid(position: str) -> bool:

    if len(position) != 5:
        return False

    if position[0].isalpha() and position[1].isdigit() and position[3].isalpha() and position[4].isdigit():

        letters = 'abcdefgh'

        if not position[0] in letters and not position[3] in letters:
            return False

        if int(position[1]) < 1 and int(position[1]) > 8 or int(position[4]) < 1 and int(position[4]) > 8:
            return False
    else:
        return False

    return True


# Gets user input data. Returns
def user_input() -> tuple[[[]], [], bool]:

    positions_list = []
    input_list = []

    print('Type the start the position of the knight and target position splitted by empty spaces:')
    while True:
        input_position = input()

        if len(input_position) <= 0:
            break

        if position_input_valid(input_position):
            position = input_position.split()

            knight_position = position[0]
            target_position = position[1]

            position = convert_positions(knight_position, target_position)

            positions_list.append(position)

            input_list.append(input_position)
            run = True
        else:
            print('Incorrect input!')
            return [[]], '', False

    return positions_list, input_list, run


# Outputs the results of the user input
def display_answer(positions_list: [[]], user_input: []) -> None:

    if len(positions_list) == 0:
        return

    i = 0
    for position in positions_list:
        
        input_position = user_input[i].split()
        # Position1(x,y), Position2(x,y)
        moves = min_moves_bfs(position[0], position[1])

        print('To get from {} to {} takes {} knight moves.'.format(
            input_position[0], input_position[1], moves))
        
        i = i + 1


if __name__ == '__main__':

    positions_list, user_input, run = user_input()

    if(run):
        display_answer(positions_list, user_input)
